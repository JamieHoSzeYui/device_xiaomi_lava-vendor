LOCAL_PATH := $(call my-dir)

PRODUCT_COPY_FILES += \
	$(call find-copy-subdir-files,*,device/xiaomi/lava-images/vendor,install/vendor/)

$(warning "Building with prebuilt vendor. Cancel build unless you know what you're doing..")
